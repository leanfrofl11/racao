const formLogin = document.querySelector('#form-login')
const msgPlace = document.getElementById('msg')
const btn = document.getElementById('HomeBtn')
formLogin.addEventListener('submit', async e => {
    e.preventDefault()
    const email = document.getElementById('inputLogin').value
    const senha = document.getElementById('inputSenha').value

    const body = { email, senha }

    try {
        await fetch('/',{
            method: "POST",
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(body)}
        ).then((response) => {
            response.json().then((data) => {
                if (data.msg === true){
                    btn.innerHTML = '<a href="/home"><button id="home-btn"><span>Home</span></button></a>'
                    msgPlace.innerHTML = `<h4>Olá, ${data.user}</h4>`
                    formLogin.style.visibility = 'hidden'
                }
                else msgPlace.innerHTML = `<h4 style="color: red">${data.msg}</h4>`
            })
        })
    } catch (error) {
        console.error(error)
    }
})
