const clienteForm = document.querySelectorAll('.client-search');
const template = '{{#cliente}}<tr> <input type="text" name="tipo" readonly value="{{tipo}}" style="display: none;"> <input type="text" name="preco" readonly value="{{preco}}" style="display: none;"><td style="width: 33%"><input type="text" name="nome" readonly value="{{nome}}"></td><td><input type="text" name="email" readonly value="{{email}}"></td><td><input type="text" name="telefone" readonly value="{{telefone}}"></td><td><input type="number" name="quant" min="0" value="0"></td><td><label><input type="checkbox" name="selected" value="{{idx}}"><span class="chk"></span></label></td></tr> {{/cliente}}';


clienteForm.forEach(form => {
    form.addEventListener('submit', e => {
        e.preventDefault();
        const name = form.querySelector('.search').value;
        
        const message = form.parentNode.querySelector('.clientes');
        message.innerHTML = '<h3 class="message">Carregando...</h3>';
        fetch('/clienteSearch?name=' + encodeURI(name)).then((response)=>{//fetch api client side, then é uma promesa
            response.json().then((data)=>{
                if(data.cliente.length === 0){
                    message.innerHTML = '<h3 class="message">Cliente não encontrado!</h3>';
                } else {
                    const stringTipo = form.parentNode.parentNode.querySelector('.tipo').innerHTML;
                    const tipo = stringTipo.substring(6, stringTipo.indexOf('\n'));
                    const stringPrice = form.parentNode.parentNode.querySelector('.price').innerHTML;
                    const preco = stringPrice.substring(2, stringPrice.indexOf('/'));
                    let idx = 0;
                    const dados = {
                        cliente: [],
                        idx() {
                            return idx++;
                        }
                    };
                    data.cliente.forEach(obj => {
                        dados.cliente.push({
                            tipo,
                            preco,
                            cpf_cnpj: obj.cpf_cnpj,
                            nome: obj.nome,
                            telefone: obj.telefone,
                            email: obj.email,
                            endereco: obj.endereco,
                        });
                    });
                    const output = Mustache.render(template, dados);
                    message.innerHTML = output;
                }
            });
        });
    });
}); 


