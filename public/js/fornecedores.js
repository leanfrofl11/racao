// busca os dados dos fornecedores e mostra na table
const table = document.querySelector('#tb-fornecedores')

const deleteFornecedor = async (cnpj) => {
    try {
        await fetch(`/fornecedores/${cnpj}`,{
            method: "DELETE"
        });
        console.log('delete produto')
    } catch (error) {
        console.error(error)
    }

    setTimeout(function(){window.location.reload();},50);
}

fetch('/fornecedoresData').then((response) => {
    response.json().then((data) => {
        let count = 0
        data.fornecedoresData.map(fornecedor => {
            table.innerHTML += `<tr id="row-${count}"> <td id="nome-${count}">${fornecedor.nome}</td> <td id="cnpj-${count}">${fornecedor.cnpj}</td> <td id="endereço-${count}">${fornecedor.endereco}</td> <td id="telefone-${count}">${fornecedor.telefone}</td> <td id="email-${count}">${fornecedor.email}</td>  <td id="options-${count}"><button class="tb-btn" id="editFornecedor-${count}" onclick="editRow(${count})">Editar</button><a href="/fornecedores"><button class="tb-btn" onclick="deleteFornecedor(${fornecedor.cnpj})">Excluir</button></a></td> </tr>`
            count = count + 1
        })
    })
})

const formFornecedor = document.querySelector('#form-fornecedor')

formFornecedor.addEventListener('submit', async e => {
    e.preventDefault()
    const msg = document.querySelector('#Error');

    const nome = document.getElementById('fornecedor-input').value
    let cnpj = document.getElementById('cnpj-input').value
    const endereco = document.getElementById('address-input').value
    const telefone = document.getElementById('telefoneFornecedor').value
    const email = document.getElementById('emailFornecedor').value
    const produto = document.getElementById('produtoFornecido').value

    cnpj = cnpj.replace(/[^\d]+/g, '')

    const body = { nome, cnpj, endereco, telefone, email }

    if (!validarCNPJ(cnpj)) return msg.innerHTML = '<h3>CNPJ Inválido!</h3>'

    try {
        await fetch('/fornecedores',{
            method: "POST",
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(body)
        });

        await setTimeout(function(){window.location.reload();},100);
    } catch (error) {
        console.error(error)
    }
})

const search = async () => {
    const name = document.querySelector('#searchFornecedor').value;
    const msg = document.querySelector('#msg');

    table.innerHTML = '';

    if (name === ''){
        await fetch('/fornecedoresData').then((response) => {
            response.json().then((data) => {
                let count = 0
                table.innerHTML = ''
                data.fornecedoresData.map(fornecedor => {
                    count = count + 1
                    table.innerHTML += `<tr id="row-${count}"> <td id="nome-${count}">${fornecedor.nome}</td> <td id="cnpj-${count}">${fornecedor.cnpj}</td> <td id="endereço-${count}">${fornecedor.endereco}</td> <td id="telefone-${count}">${fornecedor.telefone}</td> <td id="email-${count}">${fornecedor.email}</td>  <td id="options-${count}"><a href="/fornecedores"><button class="tb-btn" onclick="deleteFornecedor(${fornecedor.cnpj})">Excluir</button></a> <button class="tb-btn" id="editFornecedor-${count}" onclick="editRow(${count})">Editar</button></td> </tr>`
                })
            })
        })
    } else if (name !== ''){
        await fetch('/fornecedoresData?name=' + encodeURI(name)).then(response => {
            response.json().then(data => {
                if(data.fornecedoresData.length === 0) {
                    msg.innerHTML = '<tr class="message">Cliente não encontrado!</tr>';
                } else {
                    table.innerHTML = '';
                    msg.innerHTML = '';
                    data.fornecedoresData.map(fornecedor => {
                        table.innerHTML += `<tr> <td>${fornecedor.nome}</td> <td>${fornecedor.cnpj}</td> <td>${fornecedor.endereco}</td> <td>${fornecedor.telefone}</td> <td>${fornecedor.email}</td>  <td><a href="/fornecedores"><button class="tb-btn" onclick="deleteFornecedor(${fornecedor.cnpj})">Excluir</button></a></td> </tr>`
                    })
                }
            })
        })
    }
}

// Update Info Functions

const editRow = (row) => {
    const tb = document.getElementById('tb-fornecedores');
    const tr = document.getElementById(`row-${row}`);
    const editBtn = document.getElementById(`editFornecedor-${row}`);
    const editTxt = document.getElementById('edit-txt-f');
    var options = document.getElementById(`options-${row}`);
    var cnpj = document.getElementById(`cnpj-${row}`);

    editBtn.style.color = 'white';
    editBtn.style.backgroundColor = '#3b6978';
    tr.setAttribute('contenteditable',"true");
    tr.style.backgroundColor = 'yellow';
    editTxt.style.display = 'flex';

    options.setAttribute('contenteditable', 'false');
    cnpj.setAttribute('contenteditable', 'false');

    options.innerHTML = `<button class="tb-btn" onclick="updateRow(${row})">Save</button><button class="tb-btn" onclick="window.location.reload()">Cancel</button>`
}

const updateRow = async (row) => {
    const nome = document.getElementById(`nome-${row}`).innerText
    const cnpj = document.getElementById(`cnpj-${row}`).innerText
    const endereco = document.getElementById(`endereço-${row}`).innerText
    const telefone = document.getElementById(`telefone-${row}`).innerText
    const email = document.getElementById(`email-${row}`).innerText
//  const produto = document.getElementById(`produto-${count}`).innerText

    const body = { nome, cnpj, endereco, telefone, email }

    try {
        await fetch(`/fornecedores/${cnpj}`,{
            method: "PUT",
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(body)
        });
        console.log(body)
    } catch (error) {
        console.error(error)
    }

    setTimeout(() => {
        window.location.reload()
    }, 100)
}

function validarCNPJ(s) {
    let cnpj = s.replace(/[^\d]+/g, '')

    // Valida a quantidade de caracteres
    if (cnpj.length !== 14)
        return false

    // Elimina inválidos com todos os caracteres iguais
    if (/^(\d)\1+$/.test(cnpj))
        return false

    // Cáculo de validação
    let t = cnpj.length - 2,
        d = cnpj.substring(t),
        d1 = parseInt(d.charAt(0)),
        d2 = parseInt(d.charAt(1)),
        calc = x => {
            let n = cnpj.substring(0, x),
                y = x - 7,
                s = 0,
                r = 0

            for (let i = x; i >= 1; i--) {
                s += n.charAt(x - i) * y--;
                if (y < 2)
                    y = 9
            }

            r = 11 - s % 11
            return r > 9 ? 0 : r
        }

    return calc(t) === d1 && calc(t + 1) === d2
}















