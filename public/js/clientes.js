const clientForm = document.querySelector('#search');


const template = '{{#cliente}}<div class="user"><h3 class="user-title">{{nome}}</h3> <button class="trash-can" type="submit"><img src="../imagens/trash-solid.svg" alt="Lixo para descartar um tipo de ração"></button><hr class="card-rule" /><div class="container"><div class="row"><div class="col-sm-6"><div class="form-group row"> <label class="col-lg-2 col-form-label user-label">Endereço</label><div class="col-lg-10"> <input type="text" readonly class="form-control-plaintext user-input" value="{{endereco}}" /></div></div><div class="form-group row"> <label class="col-lg-2 col-form-label user-label">Telefone</label><div class="col-lg-10"> <input type="text" readonly class="form-control-plaintext user-input" value="{{telefone}}" /></div></div></div><div class="col-sm-6"><div class="form-group row"> <label class="col-lg-2 col-form-label user-label">E-mail</label><div class="col-lg-10"> <input type="text" readonly class="form-control-plaintext user-input" value="{{email}}" /></div></div><div class="form-group row"> <label class="col-lg-2 col-form-label user-label">CPF/CNPJ</label><div class="col-lg-10"> <input type="text" name="cpfCnpj" readonly class="form-control-plaintext user-input" value="{{cpf_cnpj}}" /></div></div></div></div></div></div>{{/cliente}}'


clientForm.addEventListener('submit', e => {
    e.preventDefault();
    const name = document.querySelector('.search-input').value;
    
    const message = document.querySelector('#clientes');
    message.innerHTML = '<h3 class="message">Carregando...</h3>';
    fetch('/clienteSearch?name=' + encodeURI(name)).then(response => {
        response.json().then(data => {
            if(data.cliente.length === 0) {
                message.innerHTML = '<h3 class="message">Cliente não encontrado!</h3>';
            } else {
                const output = Mustache.render(template, data);
                message.innerHTML = output;

                // event listeners nas lixeiras
                const trashCans = document.querySelectorAll('.trash-can');
                trashCans.forEach((trashCan) => {
                    trashCan.addEventListener('click', e => {
                        e.preventDefault();
                        const cpfCnpj = trashCan.parentNode.querySelector("[name*=cpfCnpj]").value;
                        message.innerHTML = '<h3 class="message">Removendo...</h3>';
                        fetch('/clientes?cpfCnpj=' + encodeURI(cpfCnpj), {
                            method: 'DELETE'
                        }).then(response => {
                            response.json().then(data => {
                                message.innerHTML = '<h3 class="message">' + data.response + '</h3>' ;
                            });
                        });
                    });
                });
            }
        });
    });
});

/* document.querySelector(".trash-can").parentNode.lastElementChild.lastElementChild.lastElementChild.lastElementChild.lastElementChild.lastElementChild.value */
/* document.querySelectorAll(".trash-can")[1].parentNode.querySelector("[name*=cpfCnpj]").value; */


function addCliente() {
    const form = document.getElementById('form-novoCliente');
    const btn = document.getElementById('novoCliente-btn')

    if (form.style.display === 'none'){
        btn.style.color = '#3b6978';
        btn.style.backgroundColor = '#C4C4C4';
        form.style.display = 'block'
    } else if (form.style.display === 'block') {
        btn.style.color = 'white';
        btn.style.backgroundColor = '#3b6978';
        form.style.display = 'none'
    }
}