function addNew(tb_id, form_place) {
    const divForm = document.getElementById(form_place);
    let btn

    if (form_place === 'form-place-estoque'){
        btn = document.getElementById('np-btn');
    } else if (form_place === 'form-place-fornecedores') {
        btn = document.getElementById('nf-btn');
    }

    if (divForm.style.display === 'none') {
        btn.style.color = 'white';
        btn.style.backgroundColor = '#3b6978';
        divForm.style.display = 'block'
    } else if (divForm.style.display === 'block') {
        btn.style.color = '#3b6978';
        btn.style.backgroundColor = '#C4C4C4';
        divForm.style.display = 'none'
    }
}



