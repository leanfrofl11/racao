const table = document.querySelector('#tb-estoque')

const deleteProduto = async (id) => {
    try {
        await fetch(`/estoque/${id}`,{
            method: "DELETE"
        });
        console.log('delete produto')
    } catch (error) {
        console.error(error)
    }

    setTimeout(function(){window.location.reload();},30);
}

fetch('/estoqueData').then((response) => {
    response.json().then((data) => {
        let count = 0
        data.estoqueData.map(produto => {
            count = count + 1
            table.innerHTML += `<tr id="row-${count}"><td id="nome-${count}">${produto.nome}</td> <td id="quantidade-${count}">${produto.quantidade}</td> <td id="valor-${count}">${produto.valor}</td> <td id="options-${count}-e"><button class="tb-btn" id="editProduto-${count}" onclick="editRowEstoque(${count}, ${produto.material_id})">Editar</button><button class="tb-btn" onclick="deleteProduto(${produto.material_id})">Excluir</button></td></tr>`
        })
    })
})

const formEstoque = document.querySelector('#form-estoque')

formEstoque.addEventListener('submit', async e => {
    const nome = document.getElementById('produtoEstoque').value
    const quantidade = document.getElementById('quantidadeInput').value
    const valor = document.getElementById('preçoInput').value

    const body = { nome, quantidade, valor}

    try {
        await fetch('/estoque',{
            method: "POST",
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(body)
        });
    } catch (error) {
        console.error(error)
    }
})

// Update Info Functions

const editRowEstoque = (row, material_id) => {
    const tr = document.getElementById(`row-${row}`)
    const btn = document.getElementById(`editProduto-${row}`)
    const editTxt = document.getElementById('edit-txt');
    var options = document.getElementById(`options-${row}-e`)

    btn.style.color = 'white';
    btn.style.backgroundColor = '#3b6978';
    tr.setAttribute('contenteditable',"true")
    tr.style.backgroundColor = 'yellow'
    editTxt.style.display = 'flex'
    options.setAttribute('contenteditable', 'false')

    options.innerHTML = `<button class="tb-btn" onclick="updateRowEstoque(${row}, ${material_id})">Save</button><button class="tb-btn" onclick="window.location.reload()">Cancel</button>`
}

const updateRowEstoque = async (row, id) => {
    const nome = document.getElementById(`nome-${row}`).innerText
    const quantidade = document.getElementById(`quantidade-${row}`).innerText
    const valor = document.getElementById(`valor-${row}`).innerText

    const body = { nome, quantidade, valor }
    console.log(body)

    try {
        setTimeout(()=>{
        window.location.reload();
    }, 150)
        await fetch(`/estoque/${id}`,{
            method: "PUT",
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(body)
        });
        console.log(body)
    } catch (error) {
        console.error(error)
    }



}


