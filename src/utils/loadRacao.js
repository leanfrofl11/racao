var pg = require('pg');
const loadRacao = async () =>{
    var conString = "postgres://postgres:ascii@localhost:5432/Racao"
    var client = new pg.Client(conString)
    client.connect()
    let estoque = await client.query("SELECT estoque.nome, estoque.material_id from estoque")
    let racao = await client.query("select * from receitas")
    bancoracao = [{}]
        for(i=0;i<racao.rows.length;i++){
            let obj = {
                Tipo:'',
                Animais:'',
                Preco:0
            }
            
            obj["Tipo"]=racao.rows[i].nome
            obj["Animais"]=racao.rows[i].descricao
            obj["Preco"]=racao.rows[i].preco
            
            for(j=0;j<racao.rows.length;j++){
                for(k=0;k<estoque.rows.length;k++){
                    
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Soja'){
                        obj["Soja"] = racao.rows[j].quantidade
                    }
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Milho'){
                        obj["Milho"] = racao.rows[j].quantidade
                    }
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Sugo'){
                        obj["Sugo"] = racao.rows[j].quantidade
                    }
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Proteina'){
                        obj["Proteina"] = racao.rows[j].quantidade
                    }
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Açucar'){
                        obj["Acucar"] = racao.rows[j].quantidade
                    }
                    if(racao.rows[j].componente==estoque.rows[k].material_id&&obj.Tipo==racao.rows[j].nome&&estoque.rows[k].nome=='Sal'){
                        obj["Sal"] = racao.rows[j].quantidade
                    }
                }
                bancoracao[i]=obj
            }   
        }   
        
        bancoracao = bancoracao.filter(function (a) {//função que vai filtrar os objetos duplicados
            return !this[JSON.stringify(a)] && (this[JSON.stringify(a)] = true);
        }, Object.create(null))
        
        return bancoracao
}

module.exports = loadRacao