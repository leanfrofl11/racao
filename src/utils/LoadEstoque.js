var pg = require('pg');
const loadEstoque = async ()=>{
    
    var conString = "postgres://postgres:ascii@localhost:5432/Racao"
    var client = new pg.Client(conString)
    client.connect()

    let bancoestoque = {Soja : 0,Milho: 0,Sugo: 0,Proteina: 0,Sal: 0,Acucar: 0}
    const estoque =  await client.query("select * from estoque");
    for(i=0;i<estoque.rows.length;i++){
        if(estoque.rows[i].nome=='Soja'){
            bancoestoque["Soja"] = estoque.rows[i].quantidade
        }
        if(estoque.rows[i].nome=='Milho'){
            bancoestoque["Milho"] = estoque.rows[i].quantidade
        }
        if(estoque.rows[i].nome=='Proteina'){
            bancoestoque["Proteina"] = estoque.rows[i].quantidade
        }
        if(estoque.rows[i].nome=='Sugo'){
            bancoestoque["Sugo"] = estoque.rows[i].quantidade                 
        }
        if(estoque.rows[i].nome=='Açucar'){
            bancoestoque["Acucar"] = estoque.rows[i].quantidade
        }
        if(estoque.rows[i].nome=='Sal'){
            bancoestoque["Sal"] = estoque.rows[i].quantidade
        }
    }
    return bancoestoque
}
module.exports = loadEstoque