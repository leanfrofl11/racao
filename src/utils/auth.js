function validateUser(user){
    const email = typeof user.email == 'string' && user.email.trim() !== '';
    const senha = user.senha;

    if (senha.trim() !== '' && senha.trim().length >= 6){
        return email && senha;
    }
}

module.exports = { validateUser }