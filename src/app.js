const path = require('path')
const express = require('express')
const hbs = require('hbs')//nota: pra fazer o site funcionar com partials vc tem que redefinir as extensões que o nodemon exerga usando -e jbs,hbs
const bodyParser = require('body-parser')
const pg = require('pg');
const bcrypt = require('bcryptjs')
const session = require('express-session');

const loadEstoque = require('./utils/LoadEstoque.js')
const loadRacao = require('./utils/loadRacao.js');
const { isArray } = require('util');
const auth = require('./utils/auth')

const app = express()
const port = process.env.PORT || 4000 //Só o heroku consegue fazer esse negocio, a nossa maquina vai ficar com o 4000

app.set('view engine', 'hbs')//falando pro app usar view engine do hbs

const publicDirectory = path.join(__dirname,'../public')
const viewPath = path.join(__dirname,'../templates/views')
const partialsPath = path.join(__dirname,'../templates/partials')

app.use(express.static(publicDirectory))//definindo o diretorio estatico do servidor
app.set('views', viewPath)//definindo o caminho da nova pasta view(templates), com essa linha aqui nao temos o problema de ter que rodar o comando no diretorio correto
hbs.registerPartials(partialsPath)//definindo caminho dos partials para o hbs
app.use(bodyParser.urlencoded({ extended: true }))// parse application/x-www-form-urlencoded
app.use(bodyParser.json())// parse application/json

const minutos = 1000 * 60
const horas = 1000 * 60 * 60
app.use(session({
    secret: 'ynsjkdswtrja',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 8 * horas} // Quanto tempo ele fica logado: Ex.: x * minutos / x * horas
}));


//ROTA LOGIN
app.get('/', (req, res) => {
    // Se ja tiver logado n pode acessar o login
    if (req.session.loggedin) res.redirect('/home')
    res.render('login');
});

app.post('/', async (req, res) => {
    const conString = "postgres://postgres:ascii@localhost:5432/Racao";
    const client = new pg.Client(conString);
    client.connect();

    if(auth.validateUser(req.body)) {
        try {
            let { rows } = await client.query("SELECT * FROM users WHERE email = $1", [req.body.email]);
            if (rows[0].email === req.body.email) {
                const validSenha = await bcrypt.compare(req.body.senha, rows[0].senha);
                if(!validSenha) return res.json({ msg: 'Senha Inválida!' })

                // Usuário Logado Com Sucesso
                req.session.loggedin = true;
                res.json({ msg: true, user: rows[0].nome })
                // res.redirect('/home') era pra funcionar
            } else {
                res.json({
                    msg: 'Senha Inválida!'
                })
            }
        } catch (e) {
            res.json({
                msg: 'E-mail/Senha Inválidx(s)!'
            })
        }
    } else {
        res.json({
            msg: 'E-mail/Senha Inválidx(s)!'
        })
    }
})

//ROTA HOME
app.get('/home', (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        res.render('home')
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
});

// ADD UM NOVO USUARIO
app.post('/users/newUser', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        if(auth.validateUser(req.body)) {
            try {
                let { rows } = await client.query("SELECT email FROM users WHERE email = $1", [req.body.email]);
                if (rows[0].email === req.body.email){
                    res.json({
                        msg: "email ja em uso",
                    })
                }
            } catch (e) {
                res.json({
                    msg: "tudo certo"
                })

                const { email } = req.body;
                const { senha } = req.body;

                bcrypt.hash(senha, 6).then(async (hash)=> {
                    const { rows } = await client.query("SELECT MAX(user_id) FROM users");
                    const lastId = rows[0].max

                    const newUser = await client.query("INSERT INTO users VALUES ($1, $2, $3)",
                        [lastId+1 ,email, hash])
                })
            }
        } else {
            res.json({
                msg: "invalid user"
            })
            console.log('Error')
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
})

// ROTA ESTOQUE
app.get('/estoque', (req, res) => {
    if (req.session.loggedin){
        res.render('Estoque')
    } else {
        res.redirect('/')
    }
});

app.get('/estoqueData', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        let { rows } = await client.query("SELECT * FROM estoque");
        return res.send({
            estoqueData: rows
        });
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
});

// Insere um novo fornecedor no bd
app.post('/estoque', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        try {
            const { nome } = req.body;
            const { quantidade } = req.body;
            const { valor } = req.body;
            const newProduto = await client.query("INSERT INTO estoque(nome, valor, quantidade) VALUES($1, $2, $3) RETURNING material_id",
                [nome, valor, quantidade]);

            const newlyCreatedUserId = newProduto.rows[0].material_id;
        } catch (error) {
            console.error(error)
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
})

// Atualiza as informações
app.put("/estoque/:id", async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        try {
            const { id } = req.params;
            const { nome } = req.body;
            const { quantidade } = req.body;
            const { valor } = req.body;

            const update = await client.query("UPDATE estoque SET nome = $1, quantidade = $2, valor = $3 WHERE material_id = $4", [nome, quantidade, valor, id])

        } catch (error) {
            console.error(error)
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
})

app.delete('/estoque/:id', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        try {
            const { id } = req.params
            const deleteProduto = await client.query("DELETE FROM estoque WHERE material_id = $1", [id]);
            res.json('Deleted successfully!')
        } catch (error) {
            console.error(error)
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
});

// ROTA FORNECEDORES

app.get('/fornecedores', (req, res) => {
    if (req.session.loggedin){
        res.render('fornecedores')
    } else {
        res.redirect('/')
    }
});

app.get('/fornecedoresData', async (req, res) => {
    if (req.session.loggedin){
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();

        if (!req.query.name) {
            let { rows } = await client.query("SELECT * FROM fornecedores");
            return res.send({
                fornecedoresData: rows
            });
        } else {
            try{
                let { rows } = await client.query("SELECT * FROM fornecedores WHERE nome LIKE '%"+req.query.name+"%'");
                return res.json({
                    fornecedoresData: rows
                });
            } catch (e) {
                return res.json({
                    fornecedoresData: []
                });
            }
        }
    } else {
        res.redirect('/')
    }
})

// Insere um novo fornecedor no bd
app.post('/fornecedores', async (req, res) => {
    if (req.session.loggedin){
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        try {
            const { cnpj } = req.body;
            const { nome } = req.body;
            const { telefone } = req.body;
            const { email } = req.body;
            const { endereco } = req.body;
            const newFornecedor = await client.query("INSERT INTO fornecedores VALUES($1, $2, $3, $4, $5)",
                [cnpj, nome, telefone, email, endereco]);

            res.json(newFornecedor)
        } catch (error) {
            console.error(error)
        }
    } else {
        res.redirect('/')
    }
})

// Atualiza as informações
app.put("/fornecedores/:cnpj", async (req, res) => {
    if (req.session.loggedin){
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();
        try {
            const { cnpj } = req.body;
            const { nome } = req.body;
            const { endereco } = req.body;
            const { telefone } = req.body;
            const { email } = req.body
            const update = await client.query("UPDATE fornecedores SET nome = $1, telefone = $2, email = $3, endereco = $4 WHERE cnpj = $5", [nome, telefone, email, endereco, cnpj])
            res.json("Updated successfully!")
        } catch (error) {
            console.error(error)
        }
    } else {
        res.redirect('/')
    }

})

// deleta um fornecedor do bd
app.delete("/fornecedores/:cnpj", async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        const conString = "postgres://postgres:ascii@localhost:5432/Racao";
        const client = new pg.Client(conString);
        client.connect();

        try {
            const { cnpj } = req.params
            const deleteFornecedor = await client.query("DELETE FROM fornecedores WHERE cnpj = $1", [cnpj]);
            res.json('Deleted successfully!')
        } catch (error) {
            console.error(error)
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
})


//ROTA CLIENTES
app.get('/clientes', async (req, res) => {
    if (req.session.loggedin){
        res.render('clientes')
    } else {
        res.redirect('/')
    }
});

app.get('/clienteSearch', async (req, res) => {
    if (req.session.loggedin){
        if(!req.query.name) {
            return res.json({
                cliente: []
            });
        }
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();
        try{
            let { rows } = await client.query("select * from cliente where nome LIKE '%"+req.query.name+"%'");
            return res.json({
                cliente: rows
            });
        } catch (e) {
            return res.json({
                cliente: []
            });
        }
    } else {
        res.redirect('/')
    }

});

app.post('/clientes', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();

        try {
            const { cpfCnpj: cpf_cnpj } = req.body;
            const { nome } = req.body;
            const { tel:telefone } = req.body;
            const { address:endereco } = req.body;
            const { email } = req.body;
            const answer = await client.query("INSERT INTO cliente VALUES($1, $2, $3, $4, $5)",[cpf_cnpj, nome, telefone, email, endereco]);
            res.redirect('/clientes');
        } catch(e) {
            res.redirect('/clientes');
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
});

app.delete('/clientes', async (req, res) => {
    if (req.session.loggedin){ // Se tiver logado pode acessar
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();
        try{
            const { cpfCnpj: cpf_cnpj } = req.query;
            const answer =  await client.query("DELETE FROM cliente WHERE cpf_cnpj = $1", [cpf_cnpj]);

            return res.json({
                response: 'Cliente removido com sucesso!'
            });
        }catch(e) {
            return res.json({
                response: 'Erro ao remover cliente'
            });
        }
    } else { // Se n, vai pra page login
        res.redirect('/')
    }
});

//ROTA RAÇÃO
app.get('/vendas',async (req, res) => {
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao"
        var client = new pg.Client(conString)
        client.connect()

        let bancoestoque =  await loadEstoque()//carregando estoque e reformulando ele pra rodar aqui

        let bancoclientes =  await client.query("SELECT * FROM cliente")//carregando clientes
        bancoclientes = bancoclientes.rows

        let bancoderacao = await loadRacao()//carregando receitas e reformulando ele pra rodar aqui
        if(!bancoderacao[0].Tipo) {
            return res.render('vendas');
        }
        for(i=0; i< bancoderacao.length;i++){//calculando a quantidade fabricavel de cada ração e colocando no vetor
            //da Ração X,Y,Z a gente tem que descobrir quem é o elem limitante
            //ParseFloat vai deixar apenas duas casas decimais dps da virgula
            //multiplica por 1000 pq eles vem do banco estoque que esta em Kilos
            //Colocando um valor alto como default quando o valor n esta definido no banco de receitas para a comparação nao dar errado


            if(bancoderacao[i].Soja==undefined){
                var Soja= 999999999
            }else{
                var Soja= parseFloat((bancoestoque.Soja*1000/bancoderacao[i].Soja).toFixed(2))
            }
            if(bancoderacao[i].Milho==undefined){
                var Milho = 999999999
            }else{
                var Milho= parseFloat((bancoestoque.Milho*1000/bancoderacao[i].Milho).toFixed(2))
            }
            if(bancoderacao[i].Proteina==undefined){
                var Proteina = 999999999
            }else{
                var Proteina= parseFloat((bancoestoque.Proteina*1000/bancoderacao[i].Proteina).toFixed(2))
            }
            if(bancoderacao[i].Sugo==undefined){
                var Sugo = 999999999
            }else{
                var Sugo= parseFloat((bancoestoque.Sugo*1000/bancoderacao[i].Sugo).toFixed(2))
            }
            if(bancoderacao[i].Sal==undefined){
                var Sal = 999999999
            }else{
                var Sal= parseFloat((bancoestoque.Sal*1000/bancoderacao[i].Sal).toFixed(2))
            }
            if(bancoderacao[i].Acucar==undefined){
                var Acucar = 999999999
            }else{
                var Acucar= parseFloat((bancoestoque.Acucar*1000/bancoderacao[i].Acucar).toFixed(2))
            }

            if(Soja <= Milho&&Sugo&&Proteina&&Sal&&Acucar){
                bancoderacao[i].Quantidade=Soja
            }else if(Milho <= Sugo&&Proteina&&Sal&&Acucar){
                bancoderacao[i].Quantidade=Milho
            }else if(Sugo <= Proteina&&Sal&&Acucar){
                bancoderacao[i].Quantidade= Sugo
            }else if(Proteina <= Sal&&Acucar){
                bancoderacao[i].Quantidade=Proteina
            }else if(Sal<=Acucar){
                bancoderacao[i].Quantidade=Sal
            }else{
                bancoderacao[i].Quantidade=Acucar
            }
        }

        
        res.render('vendas',{
            bancoderacao,
            bancoestoque,
            bancoclientes
        })
    } else {
        res.redirect('/')
    }
})

//ROTA REMOVER RAÇÃO
app.get('/removerRacao/:tipo', async (req,res)=>{//pegando o tipo da ração que tem que ser removido pela url
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao"
        var client = new pg.Client(conString)
        client.connect()

        await client.query("DELETE FROM receitas WHERE nome = $1", [req.params.tipo])

        res.redirect('/vendas')
    } else {
        res.redirect('/')
    }
})

//ROTA ADICIONAR CARRINHO
app.post('/addcarrinho', async (req,res)=> {
    if (req.session.loggedin){
        //desse jeito o usuario tem que salvar ração por ração aqui,
        //na hora de inserir no banco tem que ir agrupando eles,
        //n vou fazer isso no arquivo agora pq dps a gente vai trocar por banco,
        //da uma olhada no arquivo carrinho e da uma testada pra entender
        var conString = "postgres://postgres:ascii@localhost:5432/Racao"
        var client = new pg.Client(conString)
        client.connect();
        await client.query("DELETE FROM carrinho")//garantindo que esse carrinho seja unico deletando tudo o que havia nele antes
        if(Array.isArray(req.body.nome)){
            for(i=0;i<req.body.selected.length;i++){
                let index = req.body.selected[i]//no vetor selected vamos ter os indices dos clientes que vao estar realizando compras
                let obj = {
                    tipo: req.body.tipo[index],
                    nome: req.body.nome[index],
                    email: req.body.email[index],
                    telefone: req.body.telefone[index],
                    quantidade: req.body.quant[index],
                    preco: req.body.preco[index]
                }
                await client.query("INSERT INTO carrinho VALUES ($1,$2,$3,$4,$5,$6,$7)", [i,obj.tipo,obj.nome,obj.email,obj.telefone,obj.quantidade,obj.preco])
            }
        } else {
            let obj = {
                tipo: req.body.tipo,
                nome: req.body.nome,
                email: req.body.email,
                telefone: req.body.telefone,
                quantidade: req.body.quant,
                preco: req.body.preco
            }
            await client.query("INSERT INTO carrinho VALUES ($1,$2,$3,$4,$5,$6,$7)", [0,obj.tipo,obj.nome,obj.email,obj.telefone,obj.quantidade,obj.preco])
        }
        res.redirect('/vendas')
    } else {
        res.redirect('/')
    }

})

app.get('/adicionarRacao', async (req, res) => {
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();
        try {
            const { rows } = await client.query("select nome from estoque");
            res.render('adicionarRacao', {
                ingredientes: rows
            });
        } catch (e) {
            res.render('adicionarRacao', {
                ingredientes: [{nome: 'Erro'}]
            });
        }
    } else {
        res.redirect('/')
    }
});

app.post('/adicionarRacao', async (req, res) => {
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();
        let estoque;
        try {
            estoque = await client.query("select nome, material_id from estoque");
        } catch (e) {
            return res.send('Não foi possível conectar ao estoque');
        }
        const check = value => value === 'nome' || value === 'animals' || value === 'price';
        const info = {
            nome: req.body.nome,
            descricao: req.body.animals,
            preco: req.body.price
        };
        Object.keys(req.body).forEach(campo => {
            if(check(campo) || req.body[campo] === '0')
                req.body[campo] = undefined;
        });
        //body === ingredientes selecionados, info === informaçoes da receita
        try {
            const {rows} = await client.query("SELECT MAX(receita_id) from receitas");
            const receita_id = rows[0].max + 1;
            Object.keys(req.body).forEach(async campo => {
                if(req.body[campo]){
                    const obj = estoque.rows.filter(obj => obj.nome === campo);
                    // const receita_id //falta colocar no banco conforme o receita_id;
                    const { nome } = info;
                    const quantidade = Number(req.body[campo]);
                    const { material_id: componente } = obj[0];
                    const { descricao } = info;
                    const preco = Number(info.preco);
                    const answer = await client.query("INSERT INTO receitas VALUES($1, $2, $3, $4, $5, $6)",
                        [receita_id, nome, quantidade, componente, descricao, preco]);

                }
            });
            return res.redirect('/vendas');
        } catch (e) {
            return res.redirect('/vendas');
        }
    } else {
        res.redirect('/')
    }
});


//ROTA CARRINHO
app.get('/carrinho', async (req, res) => {
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();

        const carrinho = await client.query("SELECT * FROM carrinho")
        let somatoria = 0
        let tipo
        let preco
        let Dimdim

        for(i=0;i<carrinho.rows.length;i++){//simples somatoria das quantidades e filtrando informações repetidas
            somatoria+= Number(carrinho.rows[i].quantidade)
            tipo = carrinho.rows[i].tipo
            preco = carrinho.rows[i].preco
        }
        Dimdim= somatoria*preco//dinheiro que os clientes vao estar pagando no total

        res.render('carrinho',{
            carrinho: carrinho.rows,
            somatoria,
            preco,
            tipo,
            Dimdim
        })
    } else {
        res.redirect('/')
    }
});

app.post('/efetuarVenda', async (req, res)=>{//ROTA EFETUAR VENDA
    var conString = "postgres://postgres:ascii@localhost:5432/Racao";
    var client = new pg.Client(conString);
    client.connect();

    let tipo=req.body.tipo[0]//pega o tipo da ração que vai ser deduzida do estoque nas receitas


    if(Array.isArray(req.body.somatoria)){
        var quantidadetotal = Number(req.body.somatoria[0])
    }else{
        var quantidadetotal = Number(req.body.somatoria)
    }


    const bancoderacao = await loadRacao()
    let receita = {}

    if(tipo==undefined||quantidadetotal==undefined){//verificando se eles existem
        console.log('Campo tipo ou somatoria nao definidos')
        res.redirect('carrinho')
    }

    for(i=0;i<bancoderacao.length;i++){//procurando no banco o tipo
        if(bancoderacao[i].Tipo==tipo){//tipo encontrado agora pegamos sua receita
            receita = {
                Soja: bancoderacao[i].Soja,
                Milho:bancoderacao[i].Milho,
                Proteina: bancoderacao[i].Proteina,
                Sugo: bancoderacao[i].Sugo,
                Sal: bancoderacao[i].Sal,
                Acucar: bancoderacao[i].Acucar
            }
            break;//saimos do for pq n precisamos mais dele
        }
    }
    let bancoestoque = await loadEstoque() //agora carregamos a informação do estoque e deduzimos dela n podemos esquecer que la a unidade esta em kilos

    if(receita.Milho==undefined){//podem existir caso que nao tenha milho na receita entao precisamos colocar um valor default
        receita.Milho=0
    }
    if(receita.Soja==undefined){
        receita.Soja=0
    }
    if(receita.Sugo==undefined){
        receita.Sugo=0
    }
    if(receita.Proteina==undefined){
        receita.Proteina=0
    }
    if(receita.Sal==undefined){
        receita.Sal=0
    }
    if(receita.Acucar==undefined){
        receita.Acucar=0
    }

    if(Number(bancoestoque.Milho-(receita.Milho/1000*quantidadetotal)<0)||Number(bancoestoque.Soja-(receita.Soja/1000*quantidadetotal)<0)||Number(bancoestoque.Proteina-(receita.Proteina/1000*quantidadetotal)<0)||Number(bancoestoque.Sugo-(receita.Sugo/1000*quantidadetotal)<0)||Number(bancoestoque.Sal-(receita.Sal/1000*quantidadetotal)<0)||Number(bancoestoque.Acucar-(receita.Acucar/1000*quantidadetotal)<0)){//Nao podemos deixar entrar valores negativos no banco estoque
        console.log('Opa! Algum(ns) clientes tao pegando mais dessa racao do que realmente pode e o estoque iria ficar negativo')
        res.redirect('carrinho')
    }

    bancoestoque = {
        Milho: Number(bancoestoque.Milho-(receita.Milho/1000*quantidadetotal)),
        Soja: Number(bancoestoque.Soja-(receita.Soja/1000*quantidadetotal)),
        Proteina: Number(bancoestoque.Proteina-(receita.Proteina/1000*quantidadetotal)),
        Sugo: Number(bancoestoque.Proteina-(receita.Proteina/1000*quantidadetotal)),
        Sal: Number(bancoestoque.Sal-(receita.Sal/1000*quantidadetotal)),
        Acucar: Number(bancoestoque.Acucar-(receita.Acucar/1000*quantidadetotal))
    }

    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Soja'",[bancoestoque.Soja])
    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Milho'",[bancoestoque.Milho])
    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Proteina'",[bancoestoque.Proteina])
    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Sugo'",[bancoestoque.Sugo])
    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Sal'",[bancoestoque.Sal])
    await client.query("UPDATE estoque set quantidade = $1 WHERE nome = 'Açucar'",[bancoestoque.Acucar])


    res.redirect('/vendas')
})

app.get('/removerCliente/:nome',async (req,res)=>{//rota que vai remover um cliente do carrinho caso o usuario queira
    if (req.session.loggedin){
        var conString = "postgres://postgres:ascii@localhost:5432/Racao";
        var client = new pg.Client(conString);
        client.connect();

        await client.query("DELETE FROM carrinho WHERE nome = $1", [req.params.nome])

        res.redirect('/carrinho')
    } else {
        res.redirect('/')
    }
})

app.listen(port,()=>{
    console.log('Server is up on port '+port+'.')
})