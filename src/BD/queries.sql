-- busca racoes disponiveis
with qtys as ( 
	select estoque.quantidade/receitas.quantidade as qty, receitas.nome, receitas.descricao,
	receitas.preco as valor from receitas 
	inner join estoque on componente=estoque.material_id

)

select  nome, min(qty), valor, descricao as disponiveis from qtys
where qty>0
group by 1, 3, 4

-- fornecedores
select * from cliente
where length (cpf_cnpj)>11

-- clientes
select * from cliente
where length (cpf_cnpj)<12

-- update last price
UPDATE estoque SET valor = 'recebe' WHERE nome = 'RECEBE';


-- telinha de estoque
with compras as(
	select produto, avg(valor) as media from transacoes	
	where compra
	group by 1
)
select nome, quantidade, valor, media from estoque
inner join compras on compras.produto=estoque.material_id
